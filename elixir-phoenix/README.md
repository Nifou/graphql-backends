# Simple GraphQL backend with Elixir, Phoenix and Absinthe

### Try

You need to have [Elixir installed](https://elixir-lang.org/install.html).

To start the server, just run the following commands:

```bash
mix local.hex
mix archive.install hex phx_new
mix deps.get
mix phx.server
```

### Development experience

  * Language: compiled
  * Live reload: ✔️
  * Easy to make a binary: ✔️
  * Stable: ✔️
  * Lightweight: ✔️
  * Difficulty: Hard (functional language)
  * Need a boilerplate to start: ✔️
