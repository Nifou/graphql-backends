defmodule AppBackendWeb.Schema do
  use Absinthe.Schema

  object :add_page_result do
    field :name, non_null(:string)
    field :success, non_null(:boolean)
  end

  query do
    @desc "Get pages"
    field :pages, list_of(non_null(:string)) do
      resolve fn _parent, _args, _resolution ->
        { :ok, Enum.filter(File.ls!("public"), fn d -> !String.starts_with?(d, ".") end) }
      end
    end
  end

  mutation do
    @desc "Create a new page"
    field :add_page, :add_page_result do
      arg :name, non_null(:string)

      resolve fn _parent, args, _resolution ->
        File.write("public/" <> args.name, "<h1>Hello world in this new page!</h1>")
        {
          :ok,
          %{
            name: args.name,
            success: true,
          }
        }
      end
    end
  end
end
