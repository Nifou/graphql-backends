# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :appbackend,
  ecto_repos: [AppBackend.Repo]

# Configures the endpoint
config :appbackend, AppBackendWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "52p5N/ztg8I+ZvmAAQnZK73xvZXjgaY5BLFrZcEfPr+DsX3PGnne8YiMD/SseY9P",
  render_errors: [view: AppBackendWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: AppBackend.PubSub,
  live_view: [signing_salt: "6GoD8m+a"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
