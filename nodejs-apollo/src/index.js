const fs = require("fs");
const { readdir } = require("fs/promises");
const express = require("express");
const { ApolloServer, gql } = require("apollo-server-express");

async function start() {
  const typeDefs = gql`
    type AddPageResult {
      name: String!
      success: Boolean
    }

    type Query {
      pages: [String!]
    }

    type Mutation {
      addPage(name: String!): AddPageResult
    }
  `;

  const resolvers = {
    Query: {
      async pages() {
        return (await readdir("public"))
          .filter((d) => !d.startsWith("."));
      },
    },
    Mutation: {
      async addPage(parent, args) {
        const { name } = args;
        let success = true;

        await fs.writeFile(
          "public/" + name,
          "<h1>Hello world in this new page!</h1>",
          (err) => { if (err) { success = false } }
        );

        return {
          name,
          success,
        };
      },
    },
  }

  const server = new ApolloServer({ typeDefs, resolvers });
  await server.start();

  const app = express();
  server.applyMiddleware({ app });
  app.use("/preview", express.static("public"));

  await new Promise(resolve => app.listen({ port: 4000 }, resolve));
  console.log("🚀 Server started at http://localhost:4000/graphql");

  return { server, app };
}

start()
