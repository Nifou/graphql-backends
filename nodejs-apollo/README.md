# Simple GraphQL backend with Node, Express and Apollo

### Try

You need to have [Node installed](https://nodejs.org/en/download/package-manager).

To start the server, just run the following commands:

```bash
yarn install
# OR npm install

yarn start
# OR npm run start
```

### Development experience

  * Language: interpreted
  * Live reload: ✔️
  * Easy to make a binary: ✖️
  * Stable: ✔️
  * Lightweight: ✔️
  * Difficulty: Easy
  * Need a boilerplate to start: ✖️
