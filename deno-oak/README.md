# Simple GraphQL backend with Deno and Oak

### Try

You need to have [Deno installed](https://deno.land/#installation).

To start the server, just run the following command:

```bash
deno run --allow-read --allow-write --allow-net src/index.js
```

### Development experience

  * Language: interpreted (script)
  * Live reload: ✔️
  * Easy to make a binary: ✔️
  * Stable: ✖️
  * Lightweight: ✔️
  * Difficulty: Medium
  * Need a boilerplate to start: ✖️
