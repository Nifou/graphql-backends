import { Application, Router, send } from "https://deno.land/x/oak/mod.ts";
import { applyGraphQL, gql, GQLError } from "https://deno.land/x/oak_graphql/mod.ts";
import { oakCors } from "https://deno.land/x/cors/mod.ts";

const app = new Application();

const types = gql`
  type AddPageResult {
    name: String!
    success: Boolean
  }

  type Query {
    pages: [String!]
  }

  type Mutation {
    addPage(name: String!): AddPageResult
  }
`;

const resolvers = {
    Query: {
      async pages() {
        let dirs = []

        for await (const dirEntry of Deno.readDir("public")) {
          if (!dirEntry.name.startsWith(".")) {
            dirs.push(dirEntry.name);
          }
        }

        return dirs;
      },
    },
    Mutation: {
      async addPage(parent, args) {
        const { name } = args
        let success = true

        const encoder = new TextEncoder();
        const data = encoder.encode("<h1>Hello world in this new page!</h1>");

        await Deno.writeFile(
          "public/" + name,
          data,
        )

        return {
          name,
          success,
        }
      }
    }
};

const GraphQLService = await applyGraphQL({
  Router,
  typeDefs: types,
  resolvers: resolvers,
})

// Allow CORS requests
app.use(
  oakCors({
    origin: "*",
  }),
);

// Server GraphQL endpoint
app.use(GraphQLService.routes(), GraphQLService.allowedMethods());

// Serve static content from `public`
app.use(async (context, next) => {
  if (
    context.request.url.pathname !== "/graphql"
    && context.request.url.pathname.startsWith("/preview")
  ) {
    await send(context, context.request.url.pathname.replace("/preview/", ""), {
      root: `${Deno.cwd()}/public`,
      index: "index.html",
    });
  } else {
    await next();
  }
});

console.log("🚀 Server started at http://localhost:4000/graphql");
await app.listen({ port: 4000 });
