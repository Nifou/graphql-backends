# Simple GraphQL backend with Rust, Actix and Async GraphQL

### Try

You need to have [Rust installed](https://rust-lang.org/tools/install).

To start the server, just run the following command:

```bash
cargo run
```

### Development experience

  * Language: compiled
  * Live reload: ✔️
  * Easy to make a binary: ✔️
  * Stable: ✔️
  * Lightweight: ✖️
  * Difficulty: Easy
  * Need a boilerplate to start: ✖️
