use actix_web::{guard, web, App, HttpResponse, HttpServer, Result};
use async_graphql_actix_web::{Request, Response};
use async_graphql::{
    EmptySubscription, Schema, SimpleObject, Context, Object,
    http::{playground_source, GraphQLPlaygroundConfig},
    extensions::ApolloTracing,
};
use std::fs;

pub struct QueryRoot;
pub struct MutationRoot;

#[derive(SimpleObject)]
struct AddPageResult {
    name: String,
    success: bool,
}

#[Object]
impl QueryRoot {
    async fn pages(
        &self,
        _: &Context<'_>,
    ) -> Vec<String> {
        fs::read_dir("public")
            .unwrap()
            .map(|d| d.unwrap().file_name().into_string().unwrap())
            .filter(|d| !d.starts_with("."))
            .collect::<Vec<String>>()
    }
}

#[Object]
impl MutationRoot {
    async fn add_page(&self, name: String) -> AddPageResult {
        fs::write(format!("public/{}", name), "<h1>Hello world in this new page!</h1>").unwrap();

        AddPageResult {
            name,
            success: true,
        }
    }
}

pub type GraphQLSchema = Schema<QueryRoot, MutationRoot, EmptySubscription>;

async fn index(schema: web::Data<GraphQLSchema>, req: Request) -> Response {
    schema.execute(req.into_inner()).await.into()
}

async fn index_playground() -> Result<HttpResponse> {
    Ok(HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(playground_source(
            GraphQLPlaygroundConfig::new("/graphql").subscription_endpoint("/graphql"),
        )))
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let schema = Schema::build(QueryRoot, MutationRoot, EmptySubscription)
        .extension(ApolloTracing)
        .finish();

    println!("🚀 Server started at http://localhost:4000/graphql");

    HttpServer::new(move || {
        let cors = actix_cors::Cors::default()
              .allow_any_origin()
              .allow_any_method()
              .allow_any_header();

        App::new()
            .data(schema.clone())
            .wrap(cors)
            .service(actix_files::Files::new("/preview", "public").show_files_listing())
            .service(web::resource("/graphql").guard(guard::Post()).to(index))
            .service(web::resource("/graphql").guard(guard::Get()).to(index_playground))
    })
    .bind("127.0.0.1:4000")?
    .run()
    .await
}
