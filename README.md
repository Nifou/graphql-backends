# A simple project to try some GraphQL backends

The goal of this project is to try different GraphQL backends with the exactly
same frontend. The application just creates a new page and gives a link to this page.
To try them, you need to start the frontend by running (in the `frontend`
directory):

```bash
yarn install
# OR npm install

yarn dev
# OR npm run dev
```

Then, you need to start the backend you want by reading its `README.md` file and
following the `Try` instructions.
Finally, navigate to the frontend url: `http://localhost:3000`.

Current backends:

|              Language              |                  Server                  |                                GraphQL server                                |
|------------------------------------|------------------------------------------|------------------------------------------------------------------------------|
| [Deno (JS)](https://deno.land)     | [Oak](https://oakserver.github.io/oak)   | [Oak GraphQL](https://deno.land/x/oak_graphql)                               |
| [Elixir](https://elixir-lang.org)  | [Phoenix](https://phoenixframework.org)  | [Absinthe](https://absinthe-graphql.org)                                     |
| [Node (JS)](https://nodejs.org)    | [Express](https://expressjs.com)         | [Apollo](https://apollographql.com)                                          |
| [Python](https://python.org)       | [Django](https://djangoproject.com)      | [Graphene](https://graphene-python.org)                                      |
| [Rust](https://rust-lang.org)      | [Actix](https://actix.rs)                | [Async GraphQL](https://async-graphql.github.io/async-graphql/en/index.html) |
