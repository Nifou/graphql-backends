# Simple GraphQL backend with Python, Django and Graphene

### Try

You need to have [Python 3 installed](https://wiki.python.org/moin/BeginnersGuide/Download).

To start the server, just run the following command:

```bash
pip3 install django graphene-django
python3 manage.py runserver localhost:4000
```

### Development experience

  * Language: interpreted (script)
  * Live reload: ✔️
  * Easy to make a binary: ✖️
  * Stable: ✔️
  * Lightweight: ✖️
  * Difficulty: Hard
  * Need a boilerplate to start: ✔️
