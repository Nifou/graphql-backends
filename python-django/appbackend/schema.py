import graphene
import os
from graphene_django import DjangoObjectType

class Query(graphene.ObjectType):
    pages = graphene.List(graphene.String)

    def resolve_pages(root, info):
        return filter(lambda d: not d.startswith("."), os.listdir("public"))

class AddPage(graphene.Mutation):
    class Arguments:
        name = graphene.String()

    name = graphene.String()
    success = graphene.Boolean()

    def mutate(root, info, name):
        page = open("public/" + name, "w")
        page.write("<h1>Hello world in this new page!</h1>")
        page.close()

        success = True
        return AddPage(name=name, success=success)

class Mutation(graphene.ObjectType):
    addPage = AddPage.Field()

schema = graphene.Schema(query=Query, mutation=Mutation)
