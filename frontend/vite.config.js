import { defineConfig } from 'vite'
import Vue from '@vitejs/plugin-vue'
import Vql from 'vite-plugin-vue-gql'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    Vue(),
    Vql({
      fragments: '',
    }),
  ]
})
