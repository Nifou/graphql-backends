# Frontend

A simple frontend built using [Vue 3](https://v3.vuejs.org) and the blazing-fast
[Vite](https://vitejs.dev).
